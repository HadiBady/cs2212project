
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


public class ImageRenderer {

	private Point getXY(double lat, double lng, int mapWidth, int mapHeight) {
		int screenX= (int) Math.round((((lng+ 180) / 360) * mapWidth));
		int screenY= (int) Math.round(((((lat* -1) + 90) / 180) * mapHeight));
		return new Point(screenX, screenY);
	}
	
	//For getting the size of the jpeg map 
	BufferedImage myPicture = ImageIO.read(new File("map.jpg"));
	int mapWidth = myPicture.getWidth();
	int mapHeight = myPicture.getHeight();
	
	
	
			
	public void drawCircle(double longitude, double latitude) {
		
		int maxValue = getMaxValue();
		
		double percentage = getPercentage();
		
		int maxOvalDimension;
		if(maxValue< 10000) {
			maxOvalDimension= 20;
		}
		else if(maxValue< 50000) {
			maxOvalDimension= 30;
		}
		else if(maxValue< 100000) {
			maxOvalDimension= 50;
		}
		else {
			maxOvalDimension= 70;
		}
		int minOvalDimension= 15;
		int ovalDimension= (int)Math.round(((maxOvalDimension-minOvalDimension) * percentage) + minOvalDimension);
		
		// circle rendering
		Point2D coords= new Point2D.Double(longitude, latitude);
		// longitude and latitude values are of type double asgiven
		// from the csv coordinates file
		// ...............................................................
		// ...............................................................
		System.out.println("Coordinates: "+ coords.getX() + ", "+ coords.getY());
		Point testPoint = getXY(coords.getY(), coords.getX(), mapWidth, mapHeight);
		// Add the circle to the image
		Graphics2D editableImage= (Graphics2D) myPicture.getGraphics();
		editableImage.setColor(Color.RED);
		editableImage.setStroke(new BasicStroke(3));
		editableImage.fillOval(testPoint.x-(ovalDimension/ 2), testPoint.y-(ovalDimension/ 2), ovalDimension, ovalDimension);
	}
	// needs to be updated to actually get the maximum amount of cases;
	private int getMaxValue() {
		return 10000;
	}
	
	private double getPercentage() {
		return 0.10;
	}
}
