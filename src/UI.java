import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.*;
import javafx.scene.control.*;
//import javafx.scene.control.Button;
//import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler; 

public class UI extends Application{

	int beginning = 0;
	
	public UI() {
		launch();
	}
	
	@Override
	public void start(Stage arg0) throws Exception {
		switch (beginning) {
			case 0:
				loginscreen(arg0);
				break;
			case 1:
				mainscreen(arg0);
				break;
			default:
				System.out.println("Error unresolved condition");
		}
	}

	private void loginscreen(Stage arg0) {
		// TODO Auto-generated method stub
		Label username = new Label("User Name:");
		
		Label password = new Label("Password: ");
		
		
		Button button = new Button();
	      //Setting text to the button
	    button.setText("Submit");
	      //Setting the location of the button
	    button.setTranslateX(150);
	    button.setTranslateY(60);
		
		StackPane panel = new StackPane();
		
		panel.getChildren().add(username);
		panel.getChildren().add(password);
		panel.getChildren().add(button);
		/* another way of doing it
		 * Group root = new Group(button);
      Scene scene = new Scene(root, 595, 150, Color.BEIGE);
		 */
		Scene scene = new Scene(panel, 200,400);
		arg0.setTitle("Login");
		arg0.setScene(scene);
		arg0.show();
		
		button.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		        mainscreen(arg0);
		    }
		});

	}
	// --module-path /path/to/javafx-sdk-11/lib --add-modules javafx.controls,javafx.fxml
	
	private void mainscreen(Stage arg0) {
		// TODO Auto-generated method stub
		Label add = new Label("Add a country:");
		
		Label remove = new Label("Remove a country:");
		
		Label list = new Label("List of selected countries:");
		
		Label output = new Label("Output");
		
		Label analysisMethod = new Label("Choose analysis method");
		
		Button addcountry = new Button();
		Button removecountry = new Button();
		Button recalculate = new Button();
	      //Setting text to the button
	    addcountry.setText("+");
	    removecountry.setText("-");
	    recalculate.setText("Recalculate");
	      //Setting the location of the button
	    addcountry.setTranslateX(150);
	    addcountry.setTranslateY(60);
		
		StackPane panel = new StackPane();
		
		ObservableList<String> options = 
			    FXCollections.observableArrayList(
			        "Option 1",
			        "Option 2",
			        "Option 3"
			    );
			final ComboBox comboBox = new ComboBox(options);
		
		panel.getChildren().add(add);
		panel.getChildren().add(remove);
		panel.getChildren().add(list);
		/* another way of doing it
		 * Group root = new Group(button);
      Scene scene = new Scene(root, 595, 150, Color.BEIGE);
		 */
		Scene scene = new Scene(panel, 200,400);
		arg0.setTitle("Login");
		arg0.setScene(scene);
		arg0.show();

	}
}
